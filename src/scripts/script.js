$(document).ready(function() {
	$('body').addClass('loaded');

	// Remove delay 300ms on mobile devices
	if (typeof FastClick === 'function') {
		FastClick.attach(document.body);
	}

	//= parts/lazyload.js
	//= parts/fade-blocks.js
	//= parts/gallery.js
	//= parts/icheck.js
	//= parts/input.js
	//= parts/mobile-menu.js
	//= parts/validation.js
	//= parts/animate-details.js
	//= parts/file-upload.js
	//= parts/form-success.js

});
