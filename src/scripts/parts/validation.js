// 1. Contacts form
// 2. Vacancy form

/**
 * 1. Contacts form
 */
(function($) {
	var $form = $('#contacts-form'),
		$submit = $('#csubmit'),
		$agree = $('#cagree');

	$form.validate({
		// debug: true,
		errorClass: "error",
		highlight: function(element, errorClass, validClass) {
			$(element).closest('.js-input-holder').addClass(errorClass).removeClass(validClass);
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.js-input-holder').removeClass(errorClass).addClass(validClass);
		},
		messages: {
			cname: "enter your name",
			ccomment: "enter your question",
		}

	});

	$submit.prop('disabled', true);
	$agree.on('ifToggled', function() {
		$submit.prop('disabled', !$submit.prop('disabled'));
	});
})(jQuery);

/**
 * 2. Vacancy form
 */
(function($) {
	var $form = $('#vacancy-form'),
		$submit = $('#vsubmit'),
		$agree = $('#vagree');

	$form.validate({
		// debug: true,
		errorClass: "error",
		highlight: function(element, errorClass, validClass) {
			$(element).closest('.js-input-holder').addClass(errorClass).removeClass(validClass);
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.js-input-holder').removeClass(errorClass).addClass(validClass);
		},
		messages: {
			vname: "enter your name",
			vcomment: "enter your question",
		}

	});

	$submit.prop('disabled', true);
	$agree.on('ifToggled', function() {
		$submit.prop('disabled', !$submit.prop('disabled'));
	});
})(jQuery);

/**
 * 3. Mask
 */
(function($) {
	var $phoneInput = $('.js-phone-input');

	$phoneInput.inputmask({
		alias: 'phoneru',
	});
})(jQuery);

