(function($) {
	var $burger = $('.js-menu-toggler'),
		$menu = $('#mobile-menu');

	$burger.click(function() {
		$burger.toggleClass('header__burger_open');
		$menu.toggleClass('mobile-menu_open');
		$('#all-wrap').toggleClass('menu-open');

		return false;
	});
})(jQuery);