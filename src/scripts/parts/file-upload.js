// File uploads
(function() {
	$(".js-file-upload-trigger").click(function () {
		var $t = $(this),
			$input  = $t.prev('.js-file-upload-input');

		$input.click();
		return false;
	});

	function handleFileSelect(evt, $el) {
		var $parent = $el.closest('.js-file-upload-parent'),
			$error = $parent.find('.js-file-upload-error'),
			$fileName = $parent.find('.js-file-upload-file-name'),
			$remove = $parent.find('.js-file-upload-remove');

		// Hide error
		$error.hide();

		if (!evt || !evt.target || !evt.target.files) {
			$error.show().text('An error has occured');
			return false;
		}
		var file = evt.target.files[0];

		if (!file || !file.type) {
			$error.show().text('An error has occured');
			return false;
		}

		// Only process jpg files.
		if (!file.type.match('application\/pdf') && !file.type.match('application\/msword')) {
			$error.show().text('File must be .pdf or .docx');
			return false;
		}

		// Filter by file size
		if (!file.size || file.size > 1024 * 1024 * 2) {
			$error.show().text('File must be less than 2Mb (your file is ' + Math.round(file.size / 1024 / 1024 * 10) / 10 + 'Mb)');
			return false;
		}

		$fileName.removeClass('form__upload-descr_tip').text(file.name);
		$remove.css('display', 'inline-block');
	}


	$('.js-file-upload-input').change(function(e) {
		var $t = $(this);
		handleFileSelect(e, $t);
	});

	$('.js-file-upload-remove').click(function() {
		var $t = $(this),
			$parent = $t.closest('.js-file-upload-parent'),
			$input = $parent.find('.js-file-upload-input'),
			$fileName = $parent.find('.js-file-upload-file-name'),
			$removeInput = $parent.find('.js-file-upload-remove');

		$input.val("");
		$removeInput.hide();
		$fileName.addClass('form__upload-descr_tip').text($fileName.data('default'));

		return false;
	});
})();