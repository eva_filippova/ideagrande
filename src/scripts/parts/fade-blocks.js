// Scroll fading blocks
(function($) {
	var $element = $(".fade-block"),
		treshold = $(window).height() + 100,
		effect = 'fadeInUp';

	$element.each(function() {
		var $t = $(this),
			offset = $t.offset().top;

		$(window).scroll(function() {
			var scrollOffset = $(window).scrollTop();

			if (!$t.hasClass('shown') && scrollOffset > offset - treshold) {
				//$t.addClass('fade-block_fade');
				$t.addClass('animated shown ' + effect);

				$t.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
					$t.removeClass("animated " + effect);
				});
			}
		});
	})
})(jQuery);