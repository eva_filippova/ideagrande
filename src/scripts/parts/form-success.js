window.showSuccessMessage = function () {
	var $form = $('#contacts-form-wrap'),
		$message = $('#contacts-form-message'),
		$messageLink = $('#contacts-form-message-link'),
		blockPosition = $('.js-contacts-form-col').position().top;


	$form.addClass('contacts__form-col-wrap_message');
	$message.fadeIn(300);
	$('#all-wrap-content').animate({
		scrollTop: blockPosition
	}, 300);

	$messageLink.click(function () {
		var $input = $('.js-input-holder').find('input, textarea'),
			$fileInput = $('.js-file-upload-input'),
			$checkbox = $('.js-checkbox'),
			$uploadText = $('.js-file-upload-file-name'),
			uploadTextDefault = $uploadText.data('default'),
			$clear = $('.js-file-upload-remove');

		$form.removeClass('contacts__form-col-wrap_message');
		$message.fadeOut(300);
		$input.val('');
		$fileInput.val('');
		$uploadText.text(uploadTextDefault);
		$checkbox.iCheck('uncheck');
		$clear.hide();

		return false;
	});

};