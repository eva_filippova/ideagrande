(function($) {
	var $input = $('.js-input'),
		$inputHolder = $('.js-input-holder');

	$inputHolder.click(function() {
		var $t = $(this),
			$input = $t.find('.js-input');

		if ($input.length > 0) {
			$input.focus();
		}
	});

	$input.focus(function() {
		var $t = $(this),
			$inputHolder = $t.closest('.js-input-holder');

		$inputHolder.addClass('form__input-holder_focus');
	});

	$input.blur(function() {
		var $t = $(this),
			$inputHolder = $t.closest('.js-input-holder');

		$inputHolder.removeClass('form__input-holder_focus');
	});
})(jQuery);

