(function($) {
	var $controls = $('.js-gallery-controls-item'),
		$mainSlide = $('.js-gallery-main-slide'),
		$mainFrame = $('#gallery-main-frame');

	$controls.click(function() {
		var $t = $(this),
			id = $t.data('id');

		$controls.removeClass('active');
		$t.addClass('active');

		$mainFrame.css('height', $mainFrame.height());

		$mainSlide.hide();
		$('#gallery-main-slide_' + id).show();

		return false;
	});
})(jQuery);

